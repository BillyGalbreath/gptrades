package net.pl3x.bukkit.gptrades.listener;

import net.pl3x.bukkit.gptrades.GPTrades;
import net.pl3x.bukkit.gptrades.Logger;
import net.pl3x.bukkit.gptrades.configuration.Config;
import net.pl3x.bukkit.gptrades.configuration.Lang;
import net.pl3x.bukkit.gptrades.hook.GPHook;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.PrepareAnvilEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.AnvilInventory;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class GPListener implements Listener {
    private final GPTrades plugin;

    public GPListener(GPTrades plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onCreateCouponOnAnvil(PrepareAnvilEvent event) {
        AnvilInventory inventory = event.getInventory();

        if (inventory.getItem(1) != null) {
            return; // something is in 2nd slot
        }

        ItemStack item = inventory.getItem(0);
        if (item == null || item.getType() != Material.PAPER) {
            return; // not a coupon item
        }

        String text = inventory.getRenameText();
        if (!text.toLowerCase().endsWith(" claim blocks")) {
            return; // not complete
        }

        int claimBlocks;
        try {
            claimBlocks = Integer.valueOf(text.split(" ")[0]);
        } catch (NumberFormatException e) {
            return; // invalid number
        }

        // lets start the result with an unobtainable enchantment
        ItemStack result = item.clone();
        result.setAmount(1);
        result.addUnsafeEnchantment(Enchantment.DURABILITY, 10);

        // fix up the coupon name
        ItemMeta meta = result.getItemMeta();
        meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', Lang.COUPON_NAME
                .replace("{amount}", Integer.toString(claimBlocks))));

        // fix up the coupon lore
        List<String> lore = new ArrayList<>();
        Collections.addAll(lore, ChatColor.translateAlternateColorCodes('&', Lang.COUPON_LORE).split("\n"));

        // add trade code to coupon lore
        lore.add("");
        lore.add(ChatColor.translateAlternateColorCodes('&', "&7&oGP" + claimBlocks));

        // finalize result
        meta.setLore(lore);
        result.setItemMeta(meta);
        event.setResult(result);

        // set anvil cost
        inventory.setRepairCost(Config.COUPON_COST);
    }

    @EventHandler
    public void onTakeCouponFromAnvil(InventoryClickEvent event) {
        if (!(event.getInventory() instanceof AnvilInventory)) {
            return; // not an anvil inventory
        }

        final AnvilInventory inventory = (AnvilInventory) event.getInventory();
        if (inventory.getRepairCost() != Config.COUPON_COST) {
            return; // not our coupon
        }

        if (event.getSlot() != 2) {
            return; // not clicked result slot
        }

        ItemStack result = event.getCurrentItem();
        if (!isCoupon(result)) {
            return; // not our coupon
        }

        // ok this is our coupon. lets do some work now
        Player player = (Player) event.getWhoClicked();
        if (inventory.getRepairCost() > player.getLevel()) {
            event.setCancelled(true);
            return; // not enough levels
        }

        int claimBlocks = getCouponAmount(result);

        if (claimBlocks < 0) {
            Logger.warn("Unable to process GP code from coupon: " + player.getName() + " " + result);
            event.setCancelled(true);
            Bukkit.getScheduler().runTaskLater(plugin, () -> {
                player.closeInventory();
                player.setExp(player.getExp());
            }, 1);
            Lang.send(player, Lang.ERROR_PROCESSING_GPCODE);
            return; // cant process GP code
        }

        if (claimBlocks < 1) {
            Logger.debug("Cannot create negative coupon: " + player.getName() + " " + result);
            event.setCancelled(true);
            Bukkit.getScheduler().runTaskLater(plugin, () -> {
                player.closeInventory();
                player.setExp(player.getExp());
            }, 1);
            Lang.send(player, Lang.CANNOT_CREATE_NEGATIVE_COUPON);
            return; // not enough claim blocks to process request
        }

        if (claimBlocks > GPHook.getRemainingClaimBlocks(player)) {
            Logger.debug("Not enough claim blocks to create coupon: " + player.getName() + " " + result);
            event.setCancelled(true);
            Bukkit.getScheduler().runTaskLater(plugin, () -> {
                player.closeInventory();
                player.setExp(player.getExp());
            }, 1);
            Lang.send(player, Lang.NOT_ENOUGH_CLAIM_BLOCKS);
            return; // not enough claim blocks to process request
        }

        // subtract claim blocks
        GPHook.subtractClaimBlocks(player, claimBlocks);
        Logger.debug("Created coupon: " + player.getName() + " (" + claimBlocks + ") " + result);
        Lang.send(player, Lang.SUBTRACTED_CLAIM_BLOCKS
                .replace("{amount}", Integer.toString(claimBlocks)));

        // make sure we only use 1 paper from stack, give the rest back
        final ItemStack slot0 = inventory.getItem(0);
        if (slot0.getAmount() > 1) {
            slot0.setAmount(slot0.getAmount() - 1);
            Bukkit.getScheduler().runTaskLater(plugin,
                    () -> inventory.setItem(0, slot0), 1);
        } else {
            Bukkit.getScheduler().runTaskLater(plugin,
                    () -> inventory.setItem(0, null), 1);
        }

    }

    @EventHandler
    public void onRightClickCoupon(PlayerInteractEvent event) {
        if (event.getHand() != EquipmentSlot.HAND) {
            return; // not main hand packet
        }

        if (event.getAction() != Action.RIGHT_CLICK_AIR && event.getAction() != Action.RIGHT_CLICK_BLOCK) {
            return; // not right clicking
        }

        processRightClick(event.getPlayer(), event.getItem());
    }

    @EventHandler
    public void onRightClickCoupon(PlayerInteractAtEntityEvent event) {
        if (event.getRightClicked() == null) {
            return; // did not right click
        }

        processRightClick(event.getPlayer(), event.getPlayer().getInventory().getItemInMainHand());
    }

    public void processRightClick(Player player, ItemStack item) {
        if (player == null || item == null) {
            return; // incomplete data
        }

        if (!isCoupon(item)) {
            return; // not our coupon
        }

        int claimBlocks = getCouponAmount(item);
        if (claimBlocks < 0) {
            Logger.warn("Unable to process GP code from coupon: " + item);
            Lang.send(player, Lang.ERROR_PROCESSING_GPCODE);
            return; // cant process GP code
        }

        if (claimBlocks < 1) {
            Lang.send(player, Lang.ERROR_PROCESSING_GPCODE);
            return; // not enough claim blocks to process request
        }

        // check limit
        int limit = GPHook.getClaimBlocksLimit(player);
        int accrued = GPHook.getAccruedClaimBlocks(player);
        if (accrued + claimBlocks > limit) {
            Lang.send(player, Lang.CLAIM_BLOCKS_LIMIT
                    .replace("{accrued}", Integer.toString(accrued))
                    .replace("{limit}", Integer.toString(limit))
                    .replace("{amount}", Integer.toString(claimBlocks)));
            return; // not enough claim blocks to process request
        }

        // add claim blocks
        GPHook.addClaimBlocks(player, claimBlocks);
        Logger.debug("Added claim blocks from coupon: " + player.getName() + " (" + claimBlocks + ") " + item);
        Lang.send(player, Lang.ADDED_CLAIM_BLOCKS
                .replace("{amount}", Integer.toString(claimBlocks)));

        if (item.getAmount() > 1) {
            item.setAmount(item.getAmount() - 1);
            player.getInventory().setItemInMainHand(item);
        } else {
            player.getInventory().setItemInMainHand(null);
        }

        Bukkit.getScheduler().runTaskLater(plugin, player::updateInventory, 1);
    }

    public boolean isCoupon(ItemStack item) {
        if (!item.getEnchantments().containsKey(Enchantment.DURABILITY)) {
            return false; // not our coupon
        }

        if (item.getEnchantmentLevel(Enchantment.DURABILITY) != 10) {
            return false; // not our coupon
        }

        if (!item.hasItemMeta()) {
            return false; // not our coupon
        }

        ItemMeta meta = item.getItemMeta();
        if (!meta.hasDisplayName()) {
            return false; // not our coupon
        }

        if (!meta.hasLore()) {
            return false; // not our coupon
        }

        List<String> lore = meta.getLore();
        return lore.get(lore.lastIndexOf("") + 1)
                .startsWith("\u00a77\u00a7oGP");
    }

    public int getCouponAmount(ItemStack item) {
        List<String> lore = item.getItemMeta().getLore();
        String gpCode = lore.get(lore.lastIndexOf("") + 1);
        if (!gpCode.startsWith("\u00a77\u00a7oGP")) {
            return -1;
        }

        int claimBlocks;
        try {
            claimBlocks = Integer.valueOf(gpCode.substring(6));
        } catch (NumberFormatException e) {
            Logger.warn("Unable to process GP code from coupon: " + item);
            return -1; // cant process GP code
        }

        return claimBlocks;
    }
}
