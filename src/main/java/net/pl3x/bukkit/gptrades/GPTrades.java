package net.pl3x.bukkit.gptrades;

import net.pl3x.bukkit.gptrades.command.CmdGPTrades;
import net.pl3x.bukkit.gptrades.configuration.Config;
import net.pl3x.bukkit.gptrades.configuration.Lang;
import net.pl3x.bukkit.gptrades.listener.GPListener;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

public class GPTrades extends JavaPlugin {
    @Override
    public void onEnable() {
        Config.reload();
        Lang.reload();

        if (!getServer().getPluginManager().isPluginEnabled("GriefPrevention")) {
            Logger.error("# GriefPrevention NOT found and/or enabled!");
            Logger.error("# This plugin requires GriefPrevention to be installed and enabled!");
            return;
        }

        getServer().getPluginManager().registerEvents(new GPListener(this), this);

        getCommand("gptrades").setExecutor(new CmdGPTrades(this));

        Logger.info(getName() + " v" + getDescription().getVersion() + " enabled!");
    }

    @Override
    public void onDisable() {
        Logger.info(getName() + " disabled.");
    }

    public static GPTrades getPlugin() {
        return GPTrades.getPlugin(GPTrades.class);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&4" + getName() + " is disabled. See console log for more information."));
        return true;
    }
}
