package net.pl3x.bukkit.gptrades.hook;

import me.ryanhamshire.GriefPrevention.DataStore;
import me.ryanhamshire.GriefPrevention.GriefPrevention;
import me.ryanhamshire.GriefPrevention.PlayerData;
import net.pl3x.bukkit.gptrades.Logger;
import org.bukkit.entity.Player;

public class GPHook {
    public static int getAccruedClaimBlocks(Player player) {
        DataStore dataStore = GriefPrevention.instance.dataStore;
        if (dataStore == null) {
            Logger.warn("Something went wrong connecting to GriefPrevention's DataStore.");
            return 0;
        }

        PlayerData playerData = dataStore.getPlayerData(player.getUniqueId());
        if (playerData == null) {
            Logger.warn("Something went wrong connecting to GriefPrevention's PlayerData.");
            return 0;
        }

        int accrued = playerData.getAccruedClaimBlocks();
        Logger.debug("Get Accrued Claim Blocks for " + player.getName() + " = " + accrued);
        return accrued;
    }

    public static int getRemainingClaimBlocks(Player player) {
        DataStore dataStore = GriefPrevention.instance.dataStore;
        if (dataStore == null) {
            Logger.warn("Something went wrong connecting to GriefPrevention's DataStore.");
            return 0;
        }

        PlayerData playerData = dataStore.getPlayerData(player.getUniqueId());
        if (playerData == null) {
            Logger.warn("Something went wrong connecting to GriefPrevention's PlayerData.");
            return 0;
        }

        int remaining = playerData.getRemainingClaimBlocks();
        Logger.debug("Get Remaining Claim Blocks for " + player.getName() + " = " + remaining);
        return remaining;
    }


    public static boolean subtractClaimBlocks(Player player, int claimBlocks) {
        DataStore dataStore = GriefPrevention.instance.dataStore;
        if (dataStore == null) {
            Logger.warn("Something went wrong connecting to GriefPrevention's DataStore.");
            return false;
        }

        PlayerData playerData = dataStore.getPlayerData(player.getUniqueId());
        if (playerData == null) {
            Logger.warn("Something went wrong connecting to GriefPrevention's PlayerData.");
            return false;
        }

        int accrued = playerData.getAccruedClaimBlocks();
        int newValue = accrued - claimBlocks;
        playerData.setAccruedClaimBlocks(newValue);
        Logger.debug("Subtract Claim Blocks for " + player.getName() + " " + accrued + " - " + claimBlocks + " = " + newValue);

        return playerData.getAccruedClaimBlocks() == newValue;
    }

    public static boolean addClaimBlocks(Player player, int claimBlocks) {
        DataStore dataStore = GriefPrevention.instance.dataStore;
        if (dataStore == null) {
            Logger.warn("Something went wrong connecting to GriefPrevention's DataStore.");
            return false;
        }

        PlayerData playerData = dataStore.getPlayerData(player.getUniqueId());
        if (playerData == null) {
            Logger.warn("Something went wrong connecting to GriefPrevention's PlayerData.");
            return false;
        }

        int accrued = playerData.getAccruedClaimBlocks();
        int newValue = accrued + claimBlocks;
        playerData.setAccruedClaimBlocks(newValue);
        Logger.debug("Add Claim Blocks for " + player.getName() + " " + accrued + " + " + claimBlocks + " = " + newValue);

        return playerData.getAccruedClaimBlocks() == newValue;
    }

    public static int getClaimBlocksLimit(Player player) {
        DataStore dataStore = GriefPrevention.instance.dataStore;
        if (dataStore == null) {
            Logger.warn("Something went wrong connecting to GriefPrevention's DataStore.");
            return 0;
        }

        PlayerData playerData = dataStore.getPlayerData(player.getUniqueId());
        if (playerData == null) {
            Logger.warn("Something went wrong connecting to GriefPrevention's PlayerData.");
            return 0;
        }

        int limit = playerData.getAccruedClaimBlocksLimit();
        Logger.debug("Get Limit for " + player.getName() + " = " + limit);
        return limit;
    }
}
