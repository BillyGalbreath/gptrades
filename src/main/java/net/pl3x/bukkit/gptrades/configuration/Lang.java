package net.pl3x.bukkit.gptrades.configuration;

import net.pl3x.bukkit.gptrades.GPTrades;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;

public class Lang {
    public static String COMMAND_NO_PERMISSION;

    public static String COUPON_NAME;
    public static String COUPON_LORE;

    public static String ERROR_PROCESSING_GPCODE;
    public static String NOT_ENOUGH_CLAIM_BLOCKS;
    public static String CANNOT_CREATE_NEGATIVE_COUPON;
    public static String CLAIM_BLOCKS_LIMIT;

    public static String SUBTRACTED_CLAIM_BLOCKS;
    public static String ADDED_CLAIM_BLOCKS;

    public static String VERSION;
    public static String RELOAD;

    public static void reload() {
        GPTrades plugin = GPTrades.getPlugin();
        String langFile = Config.LANGUAGE_FILE;
        File configFile = new File(plugin.getDataFolder(), langFile);
        if (!configFile.exists()) {
            plugin.saveResource(Config.LANGUAGE_FILE, false);
        }
        FileConfiguration config = YamlConfiguration.loadConfiguration(configFile);

        COMMAND_NO_PERMISSION = config.getString("command-no-permission", "&4You do not have permission for that command!");

        COUPON_NAME = config.getString("coupon-name", "&6&o{amount} GP Claim Blocks");
        COUPON_LORE = config.getString("coupon-lore", "&7&oRight click to gain\n&7&oGP claim blocks.");

        ERROR_PROCESSING_GPCODE = config.getString("error-processing-gpcode", "&4Could not process GP code!");
        NOT_ENOUGH_CLAIM_BLOCKS = config.getString("not-enough-claim-blocks", "&4Not enough claim blocks to create that coupon!");
        CANNOT_CREATE_NEGATIVE_COUPON = config.getString("cannot-create-negative-coupon", "&4Cannot create negative or zero value coupons!");
        CLAIM_BLOCKS_LIMIT = config.getString("claim-blocks-limit", "&4You have reached your claim blocks limit! (Accrued: {accrued} + Coupon: {amount} > Limit: {limit})");

        SUBTRACTED_CLAIM_BLOCKS = config.getString("subtracted-claim-blocks", "&dSubtracted &7{amount} &dclaim blocks.");
        ADDED_CLAIM_BLOCKS = config.getString("added-claim-blocks", "&dAdded &7{amount} &dclaim blocks.");

        VERSION = config.getString("version", "&d{plugin} v{version}.");
        RELOAD = config.getString("reload", "&d{plugin} v{version} reloaded.");
    }

    public static void send(CommandSender recipient, String message) {
        if (message == null) {
            return; // do not send blank messages
        }
        message = ChatColor.translateAlternateColorCodes('&', message);
        if (ChatColor.stripColor(message).isEmpty()) {
            return; // do not send blank messages
        }

        for (String part : message.split("\n")) {
            recipient.sendMessage(part);
        }
    }
}
